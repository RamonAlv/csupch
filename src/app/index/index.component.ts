import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {

  }

  goRegistroAlumno(){
    this.route.navigate(['alumnoRegister']);
  }

  goRegistroMaestro(){
    this.route.navigate(['maestroRegister']);
  }

  goRegistroMateria(){
    this.route.navigate(['registroMateria']);
  }

}
