import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { RegistroAlumnoComponent } from './registro-alumno/registro-alumno.component';
import { RegistroMaestroComponent } from './registro-maestro/registro-maestro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistroMateriaComponent } from './registro-materia/registro-materia.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    RegistroAlumnoComponent,
    RegistroMaestroComponent,
    RegistroMateriaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
