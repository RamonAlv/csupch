import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { RegistroAlumnoComponent } from './registro-alumno/registro-alumno.component';
import { RegistroMaestroComponent } from './registro-maestro/registro-maestro.component';
import { RegistroMateriaComponent } from './registro-materia/registro-materia.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'alumnoRegister', component: RegistroAlumnoComponent },
  { path: 'maestroRegister', component: RegistroMaestroComponent },
  { path: 'registroMateria', component: RegistroMateriaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
